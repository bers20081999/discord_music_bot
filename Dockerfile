FROM python:3.9.4

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt
RUN apt update
RUN apt-get install -y ffmpeg

COPY . .

CMD ["python", "./bot.py"]
