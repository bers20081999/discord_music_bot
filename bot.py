import asyncio

import discord
from discord.ext import commands
from yt_dlp import YoutubeDL
import os
from dotenv import load_dotenv
load_dotenv()
intents = discord.Intents.default()
intents.message_content = True

Bot = commands.Bot(command_prefix='+', intents=intents)


class MusicCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.is_playing = False

        self.music_queue = []
        self.YDL_OPTIONS = {'format': 'bestaudio',
                   'noplaylist': 'True',
                   'postprocessors': [{
                       'key': 'FFmpegExtractAudio',
                       'preferredcodec': 'mp3',
                       'preferredquality': '320'
                   }]}
        self.FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5',
                  'options': '-vn'}

        self.vc = ""

    def search_yt(self, url):
        with YoutubeDL(self.YDL_OPTIONS, '--verbose --cookies=cookies.txt') as ydl:
            if 'https://www.youtube.com/' in url:
                info = ydl.extract_info(url, download=False)
            else:
                info = ydl.extract_info("ytsearch:%s" % url, download=False)['entries'][0]
            return {'source': info['url'], 'title': info['title']}

    def play_next(self):
        if len(self.music_queue) > 0:
            self.is_playing = True
            try:
                m_url = self.music_queue[0][0]['source']
            except Exception:
                return False
            self.music_queue.pop(0)

            self.vc.play(discord.FFmpegPCMAudio(m_url, **self.FFMPEG_OPTIONS), after=lambda e: self.play_next())
        else:
            self.is_playing = False

    async def play_music(self):
        if len(self.music_queue) > 0:
            self.is_playing = True

            m_url = self.music_queue[0][0]['source']

            if self.vc == "" or not self.vc.is_connected():
                self.vc = await self.music_queue[0][1].connect()

            self.music_queue.pop(0)

            self.vc.play(discord.FFmpegPCMAudio(m_url, **self.FFMPEG_OPTIONS), after=lambda e: self.play_next())
        else:
            self.is_playing = False

    @commands.command()
    async def play(self, ctx, *args):
        query = " ".join(args)

        voice_channel = discord.utils.get(ctx.guild.voice_channels, name=str(ctx.author.voice.channel))
        if voice_channel is None:
            await ctx.send("здарова йопта")
        else:
            song = self.search_yt(query)

            if type(song) == type(True):
                await ctx.send("паленая ссылка, не могу скачать")
            else:
                self.music_queue.append([song, voice_channel])
                await ctx.send(f"{ctx.author} поставил: {song['title']}")
            if not self.is_playing:
                await self.play_music()

    @commands.command()
    async def q(self, ctx):
        retval = ""
        for i in range(0, len(self.music_queue)):
            retval += self.music_queue[i][0]['title'] + "\n"
        if retval != "":
            await ctx.send(retval)
        else:
            await ctx.send("Песен в списке нет")

    @commands.command()
    async def stop(self, ctx):
        if self.vc != "":
            self.vc.stop()
            await self.play_music()
            await ctx.send(f'{ctx.author} остановил песню')

    @commands.command()
    async def leave(self, ctx):
        if self.vc != "":
            if self.vc.is_connected():
                await self.vc.disconnect()

    @commands.command()
    async def pause(self, ctx):
        if self.vc != "":
            if self.vc.is_playing():
                self.vc.pause()

    @commands.command()
    async def resume(self, ctx):
        if self.vc != "":
            if self.vc.is_paused():
                self.vc.resume()


async def setup(bot):
    await bot.add_cog(MusicCog(Bot))


asyncio.run(setup(Bot))
Bot.run(os.getenv('secret_key'))
